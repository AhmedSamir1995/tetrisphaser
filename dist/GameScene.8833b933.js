// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"src/scenes/GameScene.js":[function(require,module,exports) {
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*import {CST} from "../CST";
import {GameManager} from "../GameManager"
export*/
var GameScene =
/*#__PURE__*/
function (_Phaser$Scene) {
  _inherits(GameScene, _Phaser$Scene);

  function GameScene() {
    _classCallCheck(this, GameScene);

    return _possibleConstructorReturn(this, _getPrototypeOf(GameScene).call(this, {
      key: CST.SCENES.GAME
    }));
  }

  _createClass(GameScene, [{
    key: "init",
    value: function init() {
      this.scaleFactor = GameScene.scaleFactor;
      this.keyLeft = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
      this.keyRight = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
      this.keyDown = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN);
      this.keyUp = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
      this.keyLeft2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
      this.keyRight2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
      this.keyDown2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
      this.keyUp2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
      this.nextDown = 0;
      this.nextLeft = 0;
      this.nextRight = 0;
      this.poseChanged = false;
      this.pointerDown = false;
      this.updatePeriod = 1000;
      this.movementPeriod = 100;
      this.mouseDelta = [0, 0];
    }
  }, {
    key: "preload",
    value: function preload() {
      this.gameManager = new GameManager();
      this.startPosition = [5, 12];
      this.currentPosition = this.startPosition.slice(); //var scaleFactor = Math.fround(canvasWidth/1080);

      this.cellSize = 75;
      this.gridMargin = {
        x: 1,
        y: 2
      };
      this.gridMargin.x = 1.5; //this.game.renderer.width - 10.5 * cellSize * scaleFactor//this.game.renderer.width/2 - 5 * cellSize * scaleFactor;

      this.gridMargin.y = 1.5; //this.game.renderer.height - 18.5 * cellSize * scaleFactor;

      this.blocks = [[Phaser.GameObjects.Image]]; //setting backGround

      /*var BG = this.add.image(-canvasWidth*5,-canvasHeight*5, "unicorn_logo").setDepth(0);
      BG.setDisplaySize(canvasWidth*10, canvasHeight*10);
      BG.setOrigin(0);
      BG.tint = 0x666666;*/
      //end set background

      this.generateBackground();
      this.generateUpcoming();
      console.log("Game scene: Game Started");
      this.nextUpdate = 0; // this.add.text(50, 50,'window.inner' + 'width: ' + window.innerWidth + ', height: ' + window.innerHeight).setDepth(10);
      // this.add.text(50, 80,'window.inner' + 'width: ' + this.game.renderer.width + ', height: ' + this.game.renderer.height).setDepth(10);
      //let txt = this.add.text(50, 110,'CANVAS' + 'width: ' + canvasWidth + ', height: ' + canvasHeight).setDepth(10);

      this.scoreText = this.add.text(50, 50, "Score: " + 0).setDepth(10);
      this.mousePosTxt = this.add.text(50, 110, 'Mouse Pos: ').setDepth(10);
      this.createNewBlock();
      this.input.on('pointerdown', this.onPointerDown, this);
      this.input.on('pointermove', this.onPointermove, this);
      this.input.on('pointerup', this.onPointerup, this);
    }
  }, {
    key: "create",
    value: function create() {}
  }, {
    key: "changePose",
    value: function changePose() //changes the block rotation
    {
      this.currentPose++;
      this.currentPose %= this.numberOfPoses;
    }
  }, {
    key: "update",
    value: function update(time, deltaTime) {
      /*   if (this.nextUpdate == 0)
             this.nextUpdate = time + 1000;
         if (time > this.nextUpdate) {
             this.nextUpdate += 1000;
             this.blocks.forEach(
                 (item) => {
                     item.setPosition(item.x, item.y + 80);
                 }
             )
         }*/
      if (this.keyDown.isDown || this.keyDown2.isDown || this.mouseDelta[1] < -1 * this.cellSize) {
        if (time > this.nextDown) {
          this.nextDown = time + this.movementPeriod;
          this.clearCurrentShape();

          if (this.validateMove(0, -1)) {
            this.clearCurrentShape();
            this.move(0, -1);
          }

          this.drawCurrentShape(); //this.nextUpdate=time;
          //this.updatePeriod = 500;

          console.log('down down');
        }
      }
      /*if (this.keyDown.isUp)
      {
          //this.updatePeriod = 1000;
          console.log('down Up');
      }
      */


      if (this.keyRight.isDown || this.keyRight2.isDown || this.mouseDelta[0] > this.cellSize) {
        if (time > this.nextRight) {
          this.nextRight = time + this.movementPeriod;
          this.clearCurrentShape();

          if (this.validateMove(1, 0)) {
            this.clearCurrentShape();
            this.move(1, 0);
          }

          this.drawCurrentShape();
          console.log('right');
        }
      }

      if (this.keyLeft.isDown || this.keyLeft2.isDown || this.mouseDelta[0] < -1 * this.cellSize) {
        if (time > this.nextLeft) {
          this.nextLeft = time + this.movementPeriod;
          this.clearCurrentShape();

          if (this.validateMove(-1, 0)) {
            this.clearCurrentShape();
            this.move(-1, 0);
          }

          this.drawCurrentShape();
          console.log('left');
        }
      }

      if (this.keyUp.isDown || this.keyUp2.isDown) {
        if (this.poseChanged) return;
        this.poseChanged = true;
        this.clearCurrentShape();

        if (this.validatePoseChange()) {
          this.clearCurrentShape();
          this.changePose();
        }

        this.drawCurrentShape();
        console.log('poseRotate');
      }

      if (this.keyUp.isUp && this.keyUp2.isUp) {
        this.poseChanged = false;
      }

      if (this.nextUpdate == 0) this.nextUpdate = time + this.updatePeriod;

      if (time > this.nextUpdate) {
        this.nextUpdate += this.updatePeriod; //console.log("currentPosition" + this.currentPosition);

        console.log("Update");
        this.clearCurrentShape();

        if (this.validateMove(0, -1)) {
          this.clearCurrentShape();
          this.move(0, -1);
          this.drawCurrentShape();
        } else {
          this.drawCurrentShape();
          this.checkForFilledRows();
          this.createNewBlock();
        }

        this.drawCurrentShape();
        /**/
      }
    }
  }, {
    key: "updateCurrentShape",
    value: function updateCurrentShape() // replace the old currentShape draw with the new one
    {}
  }, {
    key: "move",
    value: function move(x, y) {
      this.currentPosition[0] += x;
      this.currentPosition[1] += y;
    }
  }, {
    key: "clearCurrentShape",
    value: function clearCurrentShape() // clears currentShape drawing
    {
      var positions = this.currentShape[this.currentPose];
      console.log(this.currentShape); //console.log("current shape "+this.currentShape[this.currentPose]);
      //console.log("current pose "+this.currentPose);
      //console.log("positions "+positions);

      for (var i = 0; i < positions.length; i++) {
        var ttt = positions[i];

        if (this.blocks[ttt[0] + this.currentPosition[0]] && this.blocks[ttt[0] + this.currentPosition[0]][ttt[1] + this.currentPosition[1]]) {
          var temp = this.blocks[ttt[0] + this.currentPosition[0]][ttt[1] + this.currentPosition[1]];
          if (temp) temp.tint = GameManager.nullColor;
          temp.tintx = GameManager.nullColor;
        }
      }
    }
  }, {
    key: "drawCurrentShape",
    value: function drawCurrentShape() {
      var positions = this.currentShape[this.currentPose];

      for (var i = 0; i < positions.length; i++) {
        var ttt = positions[i]; //console.log(ttt);

        if (this.blocks[ttt[0] + this.currentPosition[0]] != null && this.blocks[ttt[0] + this.currentPosition[0]][ttt[1] + this.currentPosition[1]] != null) {
          //console.log('move');
          var temp = this.blocks[ttt[0] + this.currentPosition[0]][ttt[1] + this.currentPosition[1]];
          temp.tint = this.currentShapeColor;
          temp.tintx = this.currentShapeColor;
        }
        /*let temp = new Phaser.GameObjects.Image();
        temp.setTint(GameManager.nullColor);*/

      }
    }
  }, {
    key: "checkForFilledRows",
    value: function checkForFilledRows() {
      for (var j = 0; j < this.blocks[0].length; j++) {
        var count = 0;
        console.log('countStart:' + count);
        count = 0;

        for (var i = 0; i < this.blocks.length; i++) {
          if (this.blocks[i][j].tintx != GameManager.nullColor) {
            count++;
          }
        }

        console.log('row(' + j + '): ' + count);

        if (count == 10) {
          this.clearRow(j);
          this.updateScore(10);
          j--;
        }
      }
    }
  }, {
    key: "updateScore",
    value: function updateScore() {
      var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 10;
      this.gameManager.score += x;
      console.log(this.gameManager.score);
      this.scoreText.setText("Score: " + this.gameManager.score.toString());
    }
  }, {
    key: "clearRow",
    value: function clearRow(ja) {
      for (var j = ja; j < this.blocks[0].length; j++) {
        var count = 0;
        console.log('countStart:' + count);
        count = 0;

        for (var i = 0; i < this.blocks.length; i++) {
          if (j < this.blocks[i].length - 1) {
            this.blocks[i][j].tint = this.blocks[i][j + 1].tintx;
            this.blocks[i][j].tintx = this.blocks[i][j + 1].tintx;
          } else {
            this.blocks[i][j].tint = GameManager.nullColor;
            this.blocks[i][j].tintx = GameManager.nullColor;
          }
        }
      }
    }
  }, {
    key: "validateMove",
    value: function validateMove() {
      var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      this.overFlowY = false;
      var valid = true;
      var positions = this.currentShape[this.currentPose];

      for (var i = 0; i < positions.length; i++) // check if position is clear
      {
        if (this.currentPosition[1] + positions[i][1] + y >= this.blocks[0].length) {
          this.overFlowY = true;
        }

        if (this.currentPosition[0] + positions[i][0] + x >= this.blocks.length || //this.currentPosition[1] + positions[i][1] + y >= this.blocks[this.currentPosition[0] + positions[i][0] + y].length ||
        this.currentPosition[0] + positions[i][0] + x < 0 || this.currentPosition[1] + positions[i][1] + y < 0) {
          valid = false;
          break;
        }

        if (this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y] != null && this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx != GameManager.nullColor) {
          //console.log('blocks: '+this.currentPosition[0] + positions[i][0] + x+', '+this.currentPosition[1] + positions[i][1] + y+': '+temp.tint);

          /*console.log(this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx +
              ", "+ GameManager.nullColor)*/
          valid = false;
          break;
        }
      } //console.log("Valid: " + valid);


      return valid;
    }
  }, {
    key: "validatePoseChange",
    value: function validatePoseChange() {
      var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      this.overFlowY = false;
      var valid = true;
      var wantedPose = this.currentPose + 1;
      wantedPose %= this.numberOfPoses;
      var positions = this.currentShape[wantedPose];

      for (var i = 0; i < positions.length; i++) // check if position is clear
      {
        if (this.currentPosition[1] + positions[i][1] + y >= this.blocks[0].length) {
          this.overFlowY = true;
        }

        if (this.currentPosition[0] + positions[i][0] + x >= this.blocks.length || //this.currentPosition[1] + positions[i][1] + y >= this.blocks[this.currentPosition[0] + positions[i][0] + y].length ||
        this.currentPosition[0] + positions[i][0] + x < 0 || this.currentPosition[1] + positions[i][1] + y < 0) {
          valid = false;
          break;
        }

        if (this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y] != null && this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx != GameManager.nullColor) {
          //console.log('blocks: '+this.currentPosition[0] + positions[i][0] + x+', '+this.currentPosition[1] + positions[i][1] + y+': '+temp.tint);

          /*console.log(this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx +
              ", "+ GameManager.nullColor)*/
          valid = false;
          break;
        }
      } //console.log("Valid: " + valid);


      return valid;
    }
  }, {
    key: "onPointerDown",
    value: function onPointerDown(pointer) {
      this.pointerDown = true;
      console.log("pointer down pose changed: " + this.poseChanged); //this.mousePosTxt.setText('Mouse Pos: '+pointer.x);
    }
  }, {
    key: "onPointermove",
    value: function onPointermove(pointer) {
      //
      console.log('pose changed: ' + this.poseChanged);

      if (this.pointerDown) {
        this.mouseDelta[0] =
        /*pointer.downX - */
        pointer.x - pointer.downX;
        this.mouseDelta[1] =
        /**/
        pointer.downY - pointer.y; //-pointer.downY;
      }

      this.mousePosTxt.setText("Mouse Pos: (" + pointer.x + ", " + pointer.y + ") pos down: (" + pointer.downX + ", " + pointer.downY + ")");
    }
  }, {
    key: "onPointerup",
    value: function onPointerup() {
      if (this.mouseDelta[0] < this.cellSize && this.mouseDelta[1] < this.cellSize) {
        this.clearCurrentShape();

        if (this.validatePoseChange()) {
          this.clearCurrentShape();
          this.changePose();
        }

        this.drawCurrentShape();
      }

      this.mouseDelta[0] = 0;
      this.mouseDelta[1] = 0;
      this.pointerDown = false;
    }
  }, {
    key: "createNewBlock",
    value: function createNewBlock() {
      //
      this.currentShape = GameManager.getRandomShape();
      this.currentShapeColor = GameManager.getRandomPalletColor();
      this.numberOfPoses = this.currentShape.length;
      this.currentPose = 0;
      this.currentPosition = this.startPosition.slice();
    }
  }, {
    key: "spawnNewBlock",
    value: function spawnNewBlock() {//
    }
  }, {
    key: "generateBackground",
    value: function generateBackground() {
      for (var i = 0; i < 10; i++) {
        this.blocks[i] = [];

        for (var j = 0; j < 18; j++) {
          var temp = this.generateBlockAt(i, j, "block"); //this.add.image(this.game.renderer.width-(i + gridMargin.x) * cellSize*scaleFactor, this.game.renderer.height-(j + gridMargin.y)* cellSize*scaleFactor, "block").setDepth(1);

          temp.setDepth(1); //temp.body.setGravityY(9.8);

          temp.setDisplaySize(this.cellSize * this.scaleFactor, this.cellSize * this.scaleFactor);
          temp.setOrigin(0);
          var randomRed = Math.random();
          var randomGreen = Math.random() * (1 - randomRed);
          var randomBlue = 1 - randomGreen - randomRed;
          var colorPalette = [0x00B1FF, 0x4DE5CF, 0xEB69FF, 0xFFDB00, 0xE71E8E];
          temp.tint = GameManager.nullColor; //randomRed * 0xff0000 + randomGreen * 0x00ff00 + randomBlue * 0x0000ff;

          temp.tintx = GameManager.nullColor; //console.log('blocks: '+i+', '+j+': '+temp.tintx);

          /*if(i>=8)
              temp.tint = 0x001100;*/
          //this.physics.add.image(gridMargin.x+i*80 ,gridMargin.y+j*80,"block")
          //temp.setPosition(temp.x,temp.y);
          //this.blocks[i*18+j] = temp;

          this.blocks[i][j] = temp;
        }
      }
    }
  }, {
    key: "generateUpcoming",
    value: function generateUpcoming() {
      /// Upcomings
      for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 11; j++) {
          if ((j + 1) % 4 == 0) continue;
          var temp = this.add.image((i + this.gridMargin.x - 1) * this.cellSize * this.scaleFactor, this.game.renderer.height - (j + this.gridMargin.y) * this.cellSize * this.scaleFactor, "block").setDepth(1);
          temp.setDisplaySize(this.cellSize * this.scaleFactor, this.cellSize * this.scaleFactor);
          temp.setOrigin(0);
          temp.tint = GameManager.nullColor; //randomRed * 0xff0000 + randomGreen * 0x00ff00 + randomBlue * 0x0000ff;
          //this.physics.add.image(gridMargin.x+i*80 ,gridMargin.y+j*80,"block")
          //temp.setPosition(temp.x,temp.y);
          //this.blocks[i*18+j] = temp;
          //this.blocks.push(temp);
        }
      } ///

    }
  }, {
    key: "generateBlockAt",
    value: function generateBlockAt(i, j, blockString) {
      var temp = this.add.image(this.game.renderer.width - (-i + 9 + this.gridMargin.x) * this.cellSize * this.scaleFactor, this.game.renderer.height - (j + this.gridMargin.y) * this.cellSize * this.scaleFactor, blockString);
      return temp;
    }
  }]);

  return GameScene;
}(Phaser.Scene);
},{}],"../../Users/Mowafi/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "57928" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../Users/Mowafi/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/scenes/GameScene.js"], null)
//# sourceMappingURL=/GameScene.8833b933.map