/*import { CST } from "./CST";

export */class Preloader extends Phaser.Scene {
    constructor()
    {
        super('Preloader');
        key:"Preloader"
    }
    preload()
    {
        this.facebook.showLoadProgress(this);
        this.facebook.once('startgame', this.startGame, this);
        
        this.load.image("title_bg","./assets/image/title_bg.jpg");
        this.load.image("play_button","./assets/image/UnicornPlayButton.png");
        this.load.image("play_button","./assets/image/UnicornBG.jpg");
        this.load.image("Post_BG","./assets/image/PostBG.jpg");
        this.load.image("unicorn_Logo","./assets/image/options_button.png");
        this.load.image("logo","./assets/image/logo.png");
        this.load.image("MO4Logo", "./assets/image/MO4 Unicorn Logo.png");
        this.load.image("block", "./assets/image/block.png");
        this.load.spritesheet("cat", "./assets/sprite/cat.png",{
            frameHeight:32,
            frameWidth:32
        });

        //this.load.audio("title_music","./assets/audio/shuinvy-childhood.mp3");

    } 
    startGame ()
    {

        this.scene.start(CST.SCENES.LOAD);
    }
}