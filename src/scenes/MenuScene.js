/*import {CST} from "../CST";
import { GameScene } from "./GameScene";
export */class MenuScene extends Phaser.Scene{
    constructor (){
        super(
            {
                key:CST.SCENES.MENU
            }
        )
    }
    init(data)
    {
        console.log(data);
        console.log("Menu scene: I got it");
    }
    preload()
    {
        
    }
    create()
    {
        var logo = this.add.image(this.game.renderer.width/2,this.game.renderer.height*.2,"MO4Logo").setDepth(1);
        logo.setScale(.3*GameScene.scaleFactor);
        
        //logo.setRandomPosition(logo.width/2,logo.height/2,this.game.renderer.width-logo.width/2,this.game.renderer.height-logo.height/2);
        this.add.image(this.game.renderer.width/2,0,"title_bg").setOrigin(.5,0).setDepth(0).setScale(GameScene.scaleFactor);
        let playButton = this.add.image(this.game.renderer.width/2,this.game.renderer.height/2-10,"play_button").setDepth(1);
        playButton.setScale(.1*GameScene.scaleFactor);
        let optionsButton = this.add.image(this.game.renderer.width/2,this.game.renderer.height/2+playButton.height/2+20,"options_button").setDepth(1).setScale(GameScene.scaleFactor);
        let hoverSprite = this.add.sprite(100*GameScene.scaleFactor,100*GameScene.scaleFactor,"cat");
        hoverSprite.setScale(2*GameScene.scaleFactor);
        hoverSprite.setVisible(false);

        
       /* this.sound.play("title_music",{
            loop: true
        });*/
        

        this.anims.create(
            {
                key:"walk",
                frameRate: 4,
                repeat: -1,
                frames: this.anims.generateFrameNumbers("cat",{
                    frames:[0,1,2,3]
                })
            }
        )
        

        playButton.setInteractive();
        playButton.on("pointerover",()=>{
            hoverSprite.x=playButton.x-optionsButton.width*.7;
            hoverSprite.y=playButton.y;
            hoverSprite.play("walk");
            console.log("Hover");
            hoverSprite.setVisible(true);
            //hoverSprite.setPosition(pla)
        });
        playButton.on("pointerout",()=>{
            console.log("Mouse out");
            hoverSprite.setVisible(false);
        });

        playButton.on("pointerdown",()=>{
            console.log("click");
            
        this.scene.start(CST.SCENES.GAME);
        });
        playButton.on("pointerup",()=>{
            console.log("click out");
        });

        
        optionsButton.setInteractive();
        optionsButton.on("pointerover",()=>{
            hoverSprite.x=optionsButton.x-optionsButton.width*.7;
            hoverSprite.y=optionsButton.y;
            console.log("Hover");
            hoverSprite.setVisible(true);
            //hoverSprite.setPosition(pla)
        });
        optionsButton.on("pointerout",()=>{
            console.log("Mouse out");
            hoverSprite.setVisible(false);
        });

        optionsButton.on("pointerdown",()=>{
            console.log("click");
        });
        optionsButton.on("pointerup",()=>{
            console.log("click out");
        });

        this.pointerSprite= this.add.sprite(100,100,"cat");
        this.pointerSprite.setScale(2);

    }

    update(data,data2)
    {
        
        this.pointerSprite.x= this.input.mousePointer.x;
        this.pointerSprite.y= this.input.mousePointer.y;
        //console.log(data2);
    }

}