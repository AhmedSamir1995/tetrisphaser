/*import {CST} from "../CST";
import {GameManager} from "../GameManager"
export*/ class GameScene extends Phaser.Scene {
    constructor() {
        super(
            {
                key: CST.SCENES.GAME
            }
        )
    }

    init() {
        this.scaleFactor=GameScene.scaleFactor;
        this.keyLeft  = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.keyRight = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);    
        this.keyDown  = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN);
        this.keyUp    = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        
        this.keyLeft2  = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.keyRight2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);    
        this.keyDown2  = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        this.keyUp2    = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);

        this.nextDown=0;
        this.nextLeft=0;
        this.nextRight=0;

        this.poseChanged=false;
        this.pointerDown=false;
        this.updatePeriod=1000;
        this.movementPeriod=100;
        this.mouseDelta=[0,0];
        
        this.mouseDownPos = [0,0];
        this.isGameOver=false;

        this.delta=[0,0];
    }
    

    preload() {

        this.gameManager = new GameManager();
        this.startPosition=[5,18];
        this.currentPosition=this.startPosition.slice();
        //var scaleFactor = Math.fround(canvasWidth/1080);
        this.cellSize = 100;
        this.gridMargin = { x: 1, y: 2 }
        this.gridMargin.x = 1.5;//this.game.renderer.width - 10.5 * cellSize * scaleFactor//this.game.renderer.width/2 - 5 * cellSize * scaleFactor;
        this.gridMargin.y = 1.5;//this.game.renderer.height - 18.5 * cellSize * scaleFactor;
        this.blocks = [[Phaser.GameObjects.Image]];
        //setting backGround
        /*var BG = this.add.image(-canvasWidth*5,-canvasHeight*5, "unicorn_logo").setDepth(0);
        BG.setDisplaySize(canvasWidth*10, canvasHeight*10);
        BG.setOrigin(0);
        BG.tint = 0x666666;*/
        //end set background
        this.generateBackground();
        this.generateUpcoming();
        console.log("Game scene: Game Started");
        this.nextUpdate = 0;
       // this.add.text(50, 50,'window.inner' + 'width: ' + window.innerWidth + ', height: ' + window.innerHeight).setDepth(10);
       // this.add.text(50, 80,'window.inner' + 'width: ' + this.game.renderer.width + ', height: ' + this.game.renderer.height).setDepth(10);
        //let txt = this.add.text(50, 110,'CANVAS' + 'width: ' + canvasWidth + ', height: ' + canvasHeight).setDepth(10);
        this.scoreText = this.add.text(50, 50,"Score: " + 0).setDepth(0);
        this.scoreText.style.setFontFamily("font1");
        this.postGameText = this.add.text(this.game.renderer.width/2,250,"CONGRATS !\n YOU SCORED " + this.gameManager.score).setOrigin(.5);
        this.postGameText.style.setFontFamily("font1");
        this.postGameText.style.setAlign('center');
        this.postGameText.style.setFontSize('35px');
        this.postGameText.setVisible(false);
        this.postGameText.setDepth(2);

        this.postGameBG = this.add.image(this.game.renderer.width/2,this.game.renderer.height/2,"Post_BG").setDepth(1);
        this.postGameBG.setAlpha(0.8);
        this.postGameBG.setDisplaySize(this.game.renderer.width,this.game.renderer.height);
        this.postGameBG.setVisible(false);
        

        
        let playButton = this.replayButton = this.add.image(this.game.renderer.width/2,this.game.renderer.height/2,"play_button").setDepth(2);
        playButton.setScale(.1*GameScene.scaleFactor);

        playButton.setInteractive();
        playButton.on("pointerover",()=>{
            playButton.setScale(.1*GameScene.scaleFactor*1.25);
        });
        playButton.on("pointerout",()=>{
            playButton.setScale(.1*GameScene.scaleFactor);
        });

        playButton.on("pointerdown",()=>{
            console.log("click");
            this.startGame();
        });

        this.startGame();
        //this.mousePosTxt = this.add.text(50, 110,'Mouse Pos: ').setDepth(10);
        //this.createNewBlock();
        this.input.on('pointerdown', this.onPointerDown,this);
        this.input.on('pointermove', this.onPointermove,this);
        this.input.on('pointerup', this.onPointerup,this);
    }

    create() {
        
    }
    changePose()//changes the block rotation
    {
        this.currentPose++;
        this.currentPose %= this.numberOfPoses;
        
    }
    update(time, deltaTime) {
     /*   if (this.nextUpdate == 0)
            this.nextUpdate = time + 1000;
        if (time > this.nextUpdate) {
            this.nextUpdate += 1000;
            this.blocks.forEach(
                (item) => {
                    item.setPosition(item.x, item.y + 80);
                }
            )
        }*/
        
        if(this.isGameOver)
            return;

        if (this.keyDown.isDown||this.keyDown2.isDown||this.mouseDelta[1]<-1*this.cellSize*this.scaleFactor)
        {         
            if (time > this.nextDown) {
                this.nextDown = time + this.movementPeriod;
            
            if(this.mouseDelta[1]<-1*this.cellSize*this.scaleFactor)
            {
                console.log("mousedownpos: " + this.mouseDownPos);
                console.log("mouseDelta: " + this.mouseDelta);
                this.mouseDownPos[1]+=this.cellSize*this.scaleFactor;
                this.mouseDelta[1]+=this.cellSize*this.scaleFactor;
            }

            this.clearCurrentShape()
            if (this.validateMove(0,-1)) {
                this.clearCurrentShape();
                this.move(0, -1); 
            }
                this.drawCurrentShape();
            //this.nextUpdate=time;
            //this.updatePeriod = 500;
            console.log('down down');
        }
        }      
        /*if (this.keyDown.isUp)
        {
            //this.updatePeriod = 1000;
            console.log('down Up');
        }
*/
        if (this.keyRight.isDown||this.keyRight2.isDown||this.mouseDelta[0]>this.cellSize*this.scaleFactor)
        {        
            if (time > this.nextRight) {
            this.nextRight = time + this.movementPeriod;

            if(this.mouseDelta[0]>this.cellSize*this.scaleFactor)
            {
                console.log("mousedownpos: " + this.mouseDownPos);
                console.log("mouseDelta: " + this.mouseDelta);
                this.mouseDownPos[0]+=this.cellSize*this.scaleFactor;
                this.mouseDelta[0]-=this.cellSize*this.scaleFactor;
            }

            this.clearCurrentShape()
            if (this.validateMove(1,0)) {
                this.clearCurrentShape();
                this.move(1, 0);
            }
                this.drawCurrentShape();
            console.log('right');
        }

        }

        if (this.keyLeft.isDown||this.keyLeft2.isDown||this.mouseDelta[0]<-1*this.cellSize*this.scaleFactor)
        {
            if (time > this.nextLeft) {
                this.nextLeft = time + this.movementPeriod;
                if(this.mouseDelta[0]<-1*this.cellSize*this.scaleFactor)
                {
                    console.log("mousedownpos: " + this.mouseDownPos);
                    console.log("mouseDelta: " + this.mouseDelta);
                    this.mouseDownPos[0]-=this.cellSize*this.scaleFactor;
                    this.mouseDelta[0]+=this.cellSize*this.scaleFactor;
                }
            this.clearCurrentShape()
            if (this.validateMove(-1,0)) {
                this.clearCurrentShape();
                this.move(-1, 0);
            }
                this.drawCurrentShape();
            console.log('left');
            }
        }
        if (this.keyUp.isDown||this.keyUp2.isDown)
        {
            if (this.poseChanged) 
                return;
                this.poseChanged=true;
            this.clearCurrentShape()
            if (this.validatePoseChange()) {
                this.clearCurrentShape();
                this.changePose();
            }
                this.drawCurrentShape();
            console.log('poseRotate');
            
        }
        if (this.keyUp.isUp&&this.keyUp2.isUp)
        {
            this.poseChanged=false;
        }

        if (this.nextUpdate == 0)
            this.nextUpdate = time + this.updatePeriod;
        if (time > this.nextUpdate) {
            this.nextUpdate = time + this.updatePeriod;
        //console.log("currentPosition" + this.currentPosition);
            console.log("Update");
            this.clearCurrentShape()
            if (this.validateMove(0,-1)) {
                this.clearCurrentShape();
                this.move(0, -1);
                this.drawCurrentShape();
            }
            else {
                this.drawCurrentShape();
                this.checkForFilledRows();
                if(this.overFlowY)
                {//lost the game
                    this.endGame();
                    return;
                }
                this.createNewBlock();
                if(!this.validateMove(0,0))
                {//lost the game
                    this.endGame();
                    return;
                }
            }
            this.drawCurrentShape();
        /**/
        }
    }
    updateCurrentShape() // replace the old currentShape draw with the new one
    {

    }
    
    move(x,y)
    {
        this.currentPosition[0]+=x;
        this.currentPosition[1]+=y;
    }
    clearCurrentShape() // clears currentShape drawing
    {
        let positions = this.currentShape[this.currentPose];
        //console.log(this.currentShape);
        //console.log("current shape "+this.currentShape[this.currentPose]);
        //console.log("current pose "+this.currentPose);
        //console.log("positions "+positions);
        for(let i=0;i<positions.length;i++)
        {
            let ttt = positions[i];
            if(this.blocks[ttt[0]+this.currentPosition[0]]&&this.blocks[ttt[0]+this.currentPosition[0]][ttt[1]+this.currentPosition[1]])
            {let temp = this.blocks[ttt[0]+this.currentPosition[0]][ttt[1]+this.currentPosition[1]];
            if(temp)
            temp.tint = GameManager.nullColor;
            temp.tintx = GameManager.nullColor;
            }
            
        }
    }
    drawCurrentShape()
    {
        let positions = this.currentShape[this.currentPose];
        for(let i=0;i<positions.length;i++)
        {
            let ttt = positions[i];
            //console.log(ttt);
            if(this.blocks[ttt[0]+this.currentPosition[0]]!=null&&this.blocks[ttt[0]+this.currentPosition[0]][ttt[1]+this.currentPosition[1]]!=null)
            {
                //console.log('move');
                let temp = this.blocks[ttt[0]+this.currentPosition[0]][ttt[1]+this.currentPosition[1]];
                temp.tint = this.currentShapeColor;
                temp.tintx = this.currentShapeColor;
            }
            /*let temp = new Phaser.GameObjects.Image();
            temp.setTint(GameManager.nullColor);*/
        }
    }
    checkForFilledRows()
    {
        for(let j=0;j<this.blocks[0].length;j++)
        {
            let count = 0;
            //console.log('countStart:'+count);
            count=0;
            for(let i =0;i<this.blocks.length;i++)
            {
                if(this.blocks[i][j].tintx!=GameManager.nullColor)
                {
                    count++;
                }
            }
                //console.log('row('+j+'): '+count);
            if(count==10)
            {
                this.clearRow(j);
                this.updateScore(10);
                j--;
            }
        }
    }
    updateScore(x=10)
    {
        this.gameManager.score+=x;
        //console.log(this.gameManager.score);
        this.scoreText.setText("Score: " + this.gameManager.score.toString());
    }
    clearRow(ja)
    {
        for(let j=ja;j<this.blocks[0].length;j++)
        {
            let count = 0;
            //console.log('countStart:'+count);
            count=0;
            for(let i =0;i<this.blocks.length;i++)
            {
                if(j<this.blocks[i].length-1)
                {   
                    this.blocks[i][j].tint=this.blocks[i][j+1].tintx;
                    this.blocks[i][j].tintx=this.blocks[i][j+1].tintx;
                }
                else
                   { 
                    this.blocks[i][j].tint=GameManager.nullColor
                    this.blocks[i][j].tintx=GameManager.nullColor
                   }

            }
        }
    }
    validateMove(x=0,y=0)
    {
        this.overFlowY=false;
        let valid = true;
        let positions = this.currentShape[this.currentPose];
        for (let i = 0; i < positions.length; i++) // check if position is clear
        {
            if(this.currentPosition[1] + positions[i][1] + y >= this.blocks[0].length)
            {
                this.overFlowY=true;
            }
            if (this.currentPosition[0] + positions[i][0] + x >= this.blocks.length ||
                //this.currentPosition[1] + positions[i][1] + y >= this.blocks[this.currentPosition[0] + positions[i][0] + y].length ||
                this.currentPosition[0] + positions[i][0] + x < 0 ||
                this.currentPosition[1] + positions[i][1] + y < 0) {
                valid = false;
                break;
            }
            if (this.blocks[this.currentPosition[0] + positions[i][0] + x][
                this.currentPosition[1] + positions[i][1] + y] !=null &&
                this.blocks[this.currentPosition[0] + positions[i][0] + x][
                this.currentPosition[1] + positions[i][1] + y].tintx != GameManager.nullColor)
                {
                    //console.log('blocks: '+this.currentPosition[0] + positions[i][0] + x+', '+this.currentPosition[1] + positions[i][1] + y+': '+temp.tint);
                    /*console.log(this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx +
                        ", "+ GameManager.nullColor)*/
                    valid = false;
                    break;
                }

        }
        //console.log("Valid: " + valid);
        return valid;
    }
    validatePoseChange(x=0,y=0)
    {
        this.overFlowY=false;
        let valid = true;
        let wantedPose=this.currentPose+1;
        wantedPose%=this.numberOfPoses;
        let positions = this.currentShape[wantedPose];
        for (let i = 0; i < positions.length; i++) // check if position is clear
        {
            if(this.currentPosition[1] + positions[i][1] + y >= this.blocks[0].length)
            {
                this.overFlowY=true;
            }
            if (this.currentPosition[0] + positions[i][0] + x >= this.blocks.length ||
                //this.currentPosition[1] + positions[i][1] + y >= this.blocks[this.currentPosition[0] + positions[i][0] + y].length ||
                this.currentPosition[0] + positions[i][0] + x < 0 ||
                this.currentPosition[1] + positions[i][1] + y < 0) {
                valid = false;
                break;
            }
            if (this.blocks[this.currentPosition[0] + positions[i][0] + x][
                this.currentPosition[1] + positions[i][1] + y] !=null &&
                this.blocks[this.currentPosition[0] + positions[i][0] + x][
                this.currentPosition[1] + positions[i][1] + y].tintx != GameManager.nullColor)
                {
                    //console.log('blocks: '+this.currentPosition[0] + positions[i][0] + x+', '+this.currentPosition[1] + positions[i][1] + y+': '+temp.tint);
                    /*console.log(this.blocks[this.currentPosition[0] + positions[i][0] + x][this.currentPosition[1] + positions[i][1] + y].tintx +
                        ", "+ GameManager.nullColor)*/
                    valid = false;
                    break;
                }

        }
        //console.log("Valid: " + valid);
        return valid;
    }
    onPointerDown(pointer)
    {
        this.pointerDown=true;
        this.mouseDownPos[0]=pointer.x;
        this.mouseDownPos[1]=pointer.y;
        console.log("pointer down pose changed: " + this.poseChanged);
        //this.mousePosTxt.setText('Mouse Pos: '+pointer.x);
    }
    onPointermove(pointer)
    {
        
        //
        //console.log('pose changed: ' + this.poseChanged);
        if(this.pointerDown)
        {
            this.mouseDelta[0]=/*pointer.downX - */pointer.x - this.mouseDownPos[0];
            this.mouseDelta[1]=/**/ this.mouseDownPos[1] - pointer.y;//-pointer.downY;
        }
        //this.mousePosTxt.setText("Mouse Pos: ("+pointer.x+", "+pointer.y+ ") pos down: ("+pointer.downX+", "+pointer.downY+")");
    }
    onPointerup()
    {
        if(Math.abs(this.mouseDelta[0])==0&&Math.abs(this.mouseDelta[1])==0)
        {
            this.clearCurrentShape()
            if (this.validatePoseChange()) {
                this.clearCurrentShape();
                this.changePose();
            }
                this.drawCurrentShape();
        }
        this.mouseDelta[0]=0;
        this.mouseDelta[1]=0;
        this.pointerDown=false;
    }
    createNewBlock()
    {
        //
        this.currentShape = GameManager.getRandomShape();
        this.currentShapeColor = GameManager.getRandomPalletColor();
        this.numberOfPoses = this.currentShape.length;
        this.currentPose=0;
        this.currentPosition=this.startPosition.slice();
    }
    spawnNewBlock()
    {
        //
    }
    startGame()
    {
        this.clearGrid();
        this.gameManager.score=0;
        this.updateScore(0);
        this.createNewBlock();
        this.isGameOver=false;
        this.replayButton.setVisible(false);
        this.postGameText.setVisible(false);
        this.postGameBG.setVisible(false);
        
        //leaderboard
        this.facebook.on('getleaderboard', this.onGetLeaderboard, this);

        this.facebook.getLeaderboard('High_scores');
    }
    endGame()
    {
        this.postGameText.text = "CONGRATS !\n YOU SCORED " + this.gameManager.score;
        this.replayButton.setVisible(true);
        this.postGameText.setVisible(true);
        this.postGameBG.setVisible(true);
        this.isGameOver=true;


        this.setScore(this.gameManager.score);
    }
    setScore(score)
    {
        this.leaderboard.setScore(score);
    }
    onGetLeaderboard(leaderboard)
    {
        this.leaderboard = leaderboard;
        this.leaderboard.on('getscores', function (scores)
        {
            console.log("Leaderboard Scores\n" + scores);
            scores.forEach(element => {
                console.log("Score record " + element.playerName + ": " + element.score);
            });
            console.log("End Leaderboard Scores\n");
    
        }, this);
    
        this.leaderboard.getScores();
    }
    clearGrid()
    {
        for (let i = 0; i < 10; i++) {
            
            for (let j = 0; j < 18; j++) {
                var temp = this.blocks[i][j];
                //temp.setDepth(1);
                //temp.body.setGravityY(9.8);
                //temp.setDisplaySize(this.cellSize*this.scaleFactor, this.cellSize*this.scaleFactor);
                //temp.setOrigin(0);
                temp.tint = GameManager.nullColor//randomRed * 0xff0000 + randomGreen * 0x00ff00 + randomBlue * 0x0000ff;
                temp.tintx = GameManager.nullColor
                //console.log('blocks: '+i+', '+j+': '+temp.tintx);
                /*if(i>=8)
                    temp.tint = 0x001100;*/
                //this.physics.add.image(gridMargin.x+i*80 ,gridMargin.y+j*80,"block")
                //temp.setPosition(temp.x,temp.y);
                //this.blocks[i*18+j] = temp;
            }
        }
    }

    generateBackground()
    {
        
        for (let i = 0; i < 10; i++) {
            this.blocks[i]=[];
            for (let j = 0; j < 18; j++) {
                var temp = this.generateBlockAt(i, j, "block");//this.add.image(this.game.renderer.width-(i + gridMargin.x) * cellSize*scaleFactor, this.game.renderer.height-(j + gridMargin.y)* cellSize*scaleFactor, "block").setDepth(1);
                temp.setDepth(1);
                //temp.body.setGravityY(9.8);
                temp.setDisplaySize(this.cellSize*this.scaleFactor, this.cellSize*this.scaleFactor);
                temp.setOrigin(0);
                var randomRed = Math.random();
                var randomGreen = Math.random()*(1-randomRed);
                var randomBlue = (1-randomGreen-randomRed);
                var colorPalette = [0x00B1FF,0x4DE5CF,0xEB69FF,0xFFDB00,0xE71E8E]
                temp.tint = GameManager.nullColor//randomRed * 0xff0000 + randomGreen * 0x00ff00 + randomBlue * 0x0000ff;
                temp.tintx = GameManager.nullColor
                //console.log('blocks: '+i+', '+j+': '+temp.tintx);
                /*if(i>=8)
                    temp.tint = 0x001100;*/
                //this.physics.add.image(gridMargin.x+i*80 ,gridMargin.y+j*80,"block")
                //temp.setPosition(temp.x,temp.y);
                //this.blocks[i*18+j] = temp;
                this.blocks[i][j] = temp;
            }
        }
    }

    generateUpcoming()
    {
        let localScaleFactor = this.scaleFactor*.6;
        /*this.add.rectangle(this.game.renderer.width-11.5*this.cellSize*localScaleFactor-this.gridMargin.x*this.cellSize*this.scaleFactor,
                            (this.gridMargin.y-.5)*this.cellSize*this.scaleFactor,
                            12*this.cellSize*localScaleFactor,
                            4*this.cellSize*localScaleFactor
                            ).once.fillColor=0x111111;
        */
        let graphics = this.add.graphics({ lineStyle: { width: 2, color: 0x0000aa }, fillStyle: { color: 0xaa0000 }});
            graphics.fillStyle(0xffb200);
        graphics.fillRect(this.game.renderer.width-10.25*this.cellSize*localScaleFactor-this.gridMargin.x*this.cellSize*this.scaleFactor,
            (this.gridMargin.y)*this.cellSize*this.scaleFactor-1.25*this.cellSize*localScaleFactor,//-0.5*this.cellSize*localScaleFactor,
            11.5*this.cellSize*localScaleFactor,
            3.5*this.cellSize*localScaleFactor
            );
            
            this.upcommingBlocks = [[Phaser.GameObjects.Image]];

        /// Upcomings
        for(let i=0;i<11;i++)
        {
            for(let j=0;j<3;j++)
            {
                if((i+1)%4==0)
                    continue;
                var temp = this.add.image(this.game.renderer.width-(i) * this.cellSize*localScaleFactor - this.gridMargin.x*this.cellSize*this.scaleFactor,
                                                 (j-1)* this.cellSize*localScaleFactor + this.gridMargin.y*this.cellSize*this.scaleFactor, "block").setDepth(1);
                temp.setDisplaySize(this.cellSize*localScaleFactor, this.cellSize*localScaleFactor);
                temp.setOrigin(0);
                temp.tint = GameManager.nullColor//randomRed * 0xff0000 + randomGreen * 0x00ff00 + randomBlue * 0x0000ff;
                //this.physics.add.image(gridMargin.x+i*80 ,gridMargin.y+j*80,"block")
                //temp.setPosition(temp.x,temp.y);
                //this.blocks[i*18+j] = temp;
                //this.blocks.push(temp);
                
            }
        }
        ///
    }

    generateBlockAt(i, j, blockString)
    {
        var temp = this.add.image(this.game.renderer.width/2-(-i +5) * this.cellSize*this.scaleFactor,
                                     this.game.renderer.height-(j + this.gridMargin.y)* this.cellSize*this.scaleFactor, blockString);
        return temp;
    }

}