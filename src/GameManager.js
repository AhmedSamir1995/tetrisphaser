/*export */class GameManager{
    constructor()
    {
        this.score = 0;
        
        this.Generator=function(){
            
        }
        this.grid=(function(){
            temp =[];
            for(let i=0;i<10;i++)
            {
                temp[i]=[];
                for(let j=0;j<18;j++)
                {
                    temp[i][j]=0;
                }
            }
        })
    }

    
    static getRandomPalletColor()
    {
        return GameManager.colorPalette[Math.round(Math.random()*(this.colorPalette.length-1))];
    }

    static getRandomShape()
    {
        return GameManager.Shapes[Math.round(Math.random()*(this.Shapes.length-1))];
    }
}
    //Static variables
    GameManager.nullColor = 0x302959;
    GameManager.colorPalette = [0x00B1FF,0x4DE5CF,0xEB69FF,0xFFDB00,0xE71E8E];
    GameManager.Shapes = [
        [//shape 1 T
            [ //pose 0
                [0,0],
                [1,0],
                [-1,0],
                [0,-1]
            ],
            [ //pose 90
                [0,0],
                [0,1],
                [0,-1],
                [1,0]
            ],
            [ //pose 180
                [0,0],
                [1,0],
                [-1,0],
                [0,1]
            ],
            [ //pose 270
                [0,0],
                [0,1],
                [0,-1],
                [-1,0]
            ]
        ],
        [//Shape 2 +
            [//pose 0 and only 0
                [0,0],
                [0,1],
                [0,-1],
                [1,0],
                [-1,0] 
            ]
        ],
        [// shape 3 L
            [//pose 0
                [0,0],
                [0,1],
                [0,-1],
                [1,-1]
            ],
            [//pose 90
                [0,0],
                [-1,0],
                [1,0],
                [1,1]
            ],
            [//pose 180
                [0,0],
                [0,1],
                [0,-1],
                [-1,1]
            ],
            [//pose 270
                [0,0],
                [1,0],
                [-1,0],
                [-1,-1]
            ]
        ],
        [//Shape 4 -
            [//pose vertical
                [0,0],
                [0,1],
                [0,-1]
            ],
            [//pose horizontal
                [0,0],
                [-1,0],
                [1,0]
            ]
        ],
        [//Shape 5 z
            [//Pose Normal
                [0,0],
                [1,0],
                [0,1],
                [-1,1]
            ],
            [//Pose Rotated
                [0,0],
                [0,-1],
                [1,0],
                [1,1]
            ]
        ],
        [//Shape 6 block 2x2
            [//Pose only
                [0,0],
                [0,1],
                [1,0],
                [1,1]
            ]
        ],
        [//Shape 7 S
            [//Pose Normal
                [0,0],
                [-1,0],
                [0,1],
                [1,1]
            ],
            [//Pose Rotated
                [0,0],
                [-1,0],
                [0,-1],
                [-1,1]
            ]
        ],
        [//Shape 8 L flipped
            [//Pose 0
                [0,0],
                [0,1],
                [0,-1],
                [-1,-1]
            ],
            [//Pose 90
                [0,0],
                [-1,0],
                [1,0],
                [1,-1]
            ],
            [//Pose 180
                [0,0],
                [0,1],
                [0,-1],
                [1,1]
            ],
            [//Pose 270
                [0,0],
                [1,0],
                [-1,0],
                [-1,1]
            ]
        ]
    ];// end shapes
