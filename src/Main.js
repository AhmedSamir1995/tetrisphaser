﻿/**@type{import("../typings/phaser")}*/
/*import { LoadScene } from "./scenes/LoadScene";
import { MenuScene } from "./scenes/MenuScene";
import { GameScene } from "./scenes/GameScene";
import { Preloader } from "./PreLoader";*/
FBInstant.initializeAsync().then(function() {
//FBInstant.initializeAsync().then(function () {

    // var config = {
    //     type: Phaser.AUTO,
    //     width: window.innerWidth,
    //     height: window.innerHeight
    // };

    // new Phaser.Game(config);
    GameScene.canvasWidth =window.innerWidth;//=500; //window.innerWidth;
    GameScene.canvasHeight =window.innerHeight;//= 930;// window.innerHeight;
    if(GameScene.canvasWidth*2<=GameScene.canvasHeight)
    {
        GameScene.canvasHeight=GameScene.canvasWidth*2;
    }
    else
    {
        if(GameScene.canvasHeight%2==1)
        GameScene.canvasHeight-=1;
        GameScene.canvasWidth=GameScene.canvasHeight/2;
    }
    GameScene.scaleFactor = (GameScene.canvasWidth/1080).toFixed(2);
console.log("initializing");
    let game = new Phaser.Game({
        type: Phaser.AUTO,

        width: window.innerWidth,
        height: window.innerHeight,
        /*physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 300 },
                debug: false
            }
        },*/
        render: {
            pixelArt: true,
            autoResize: true
            
        },

        scene: [
            Preloader,
            LoadScene,
            MenuScene,
            GameScene
        ]
    });
});
//});